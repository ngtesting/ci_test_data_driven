package im.tester.test.login.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ZenData {
    public static Object[][] LoadData(
            String host, String defaultFile, String configFile, int count) {
        // http://192.168.1.161:58849/data?config=users/test/_test01.yaml&n=100

        String url = String.format("http://%s/data?default=%s&config=%s&&n=%d",
                host, defaultFile, configFile, count);
        String jsonStr = HttpRequest.get(url);

        List<JSONObject> data = JSON.parseArray(jsonStr, JSONObject.class);

        Iterator<JSONObject> it = data.iterator();
        Object[][] arr = new Object[data.size()][2];
        int i = 0;
        while(it.hasNext()){
            String key = i + 1 + "";
            JSONObject item = it.next();

            arr[i][0] = key;
            arr[i][1] = item;

            i++;
        }

        return arr;
    }
}
