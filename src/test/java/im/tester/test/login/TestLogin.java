package im.tester.test.login;

import com.alibaba.fastjson.JSONObject;
import im.tester.test.login.utils.ZenData;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import java.util.Iterator;
import java.util.List;

@Test(testName = "登录测试")
public class TestLogin {
    private static final Logger log = Logger.getLogger(TestLogin.class);

    @Test(priority=1, testName = "登录", dataProvider="userAccount")
    public void login(String key, Object item) {
        JSONObject json = (JSONObject)item;
        String email = json.getString("email");
        String password = json.getString("password");
        String expectedTitle = json.getString("title");

        // 此处完成登录操作 ...

        // 获取页面标题
        String actualTitle = expectedTitle; // mock

        // 验证页面标题
        Assert.assertEquals(actualTitle , expectedTitle);
    }

    @DataProvider(name="userAccount")
    public Object[][] getUserAccountData() {
        Object[][] data = ZenData.LoadData(
                "127.0.0.1:8848", "","users/test/_account.yaml", 3);

        return data;
    }
}
